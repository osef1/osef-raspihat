#include "SignalHandler.h"
#include "Raspihat.h"

#include <unistd.h>  // sleep
#include <iostream>  // cout

bool displayBoardInfo(OSEF::Raspihat& hat)
{
    bool ret = false;

    std::string name = "";
    if (hat.getBoardName(name))
    {
        std::cout << "Raspihat board name " << name << std::endl;

        uint8_t maj = 0;
        uint8_t sub = 0;
        uint8_t min = 0;
        if (hat.getBoardVersion(maj, sub, min))
        {
            std::cout << "Raspihat board version " << +maj << "." << +sub << "." << +min << std::endl;

            ret = true;
        }
    }

    return ret;
}

bool checkLoopback(OSEF::Raspihat& hat, const uint32_t& relay)
{
    bool ret = false;

    uint32_t commands = 1 << (relay - 1);
    if (hat.setRelayStates(commands))
    {
        std::cout << "Relay " << relay << " ON -> ";
        uint32_t positions = 0;
        if (hat.getRelayStates(positions))
        {
            if (commands == positions)
            {
                ret = true;
                std::cout << "OK";
            }
        }
        else
        {
            std::cout << "KO!";
        }
        std::cout << std::endl;
    }
    else
    {
        hat.setRelayStates(0);
        std::cout << "Error setting Raspihat board outputs" << std::endl;
    }

    return ret;
}

bool getI2cAdapterDevice(int argc, char** argv, uint8_t& adapter, uint8_t& device)
{
    bool ret = true;

    if (argc > 1)
    {
        int32_t opt = -1;
        uint64_t tmp = 0;

        while ((opt = getopt(argc, argv, "a:d:")) != -1)
        {
            switch (opt)
            {
                case 'a':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if ( (tmp != 0) && (tmp <= 0xffUL) )
                    {
                        adapter = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
                    break;
                case 'd':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if (tmp <= 0xffUL)
                    {
                        device = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
                    break;
                default:
                    ret = false;
                    break;
            }
        }
    }

    if (not ret)
    {
        std::cout << "Usage:   " << *argv << " [-option] [argument]" << std::endl;
        std::cout << "option:  " << std::endl;
        std::cout << "         " << "-a  I2C adapter [1-255]" << std::endl;
        std::cout << "         " << "-d  I2C device [0-255]" << std::endl;
    }

    return ret;
}

int main(int argc, char** argv)
{
    int32_t ret = -1;

    OSEF::SignalHandler signal;

    if (signal.setSignalActionList({SIGINT, SIGTERM}))
    {
        uint8_t adapter = 1;
        uint8_t device = 0x60;
        if (getI2cAdapterDevice(argc, argv, adapter, device))
        {
            OSEF::Raspihat hat(adapter, device);
            std::cout << "Checking SMBus adapter " << +adapter << " device 0x" << std::hex << +device << std::dec << std::endl;

            if (displayBoardInfo(hat))
            {
                uint32_t relay = 1;
                while (checkLoopback(hat, relay) && not signal.signalReceived())
                {
                    relay++;
                    if (relay > 6)
                    {
                        relay = 1;
                    }

                    sleep(1);
                };

                ret = 0;
            }
        }
    }

    return ret;
}
