#include "Raspihat.h"
#include "Debug.h"

extern "C"
{
#include <i2c/smbus.h>  // i2c_smbus_read_i2c_block_data i2c_smbus_write_i2c_block_data
}

OSEF::Raspihat::Raspihat(const uint8_t& adp, const uint8_t& dvc)
    :device(adp, dvc),
    id(33),
    request{},
    response{}
{
    if ((device.getDevice() < static_cast<uint8_t>(0x40)) || (device.getDevice() > static_cast<uint8_t>(0x6f)))
    {
        DERR("Raspihat supports only device numbers in [0x40-0x6f] not 0x" << std::hex << +device.getDevice() << std::dec);
    }

    memset(&request[0], 0, RHF_MAX_SIZE);
    memset(&response[0], 0, RHF_MAX_SIZE);
}

bool OSEF::Raspihat::getCRC16(uint16_t& crc, const RHFrame& frame, const uint8_t& length)
{
    bool ret = false;

    if (length <= RHF_MAX_SIZE)
    {
        crc = 0xffff;

        for (size_t i = 0; i < static_cast<size_t>(length); i++)
        {
            uint16_t idx = (crc^frame[i]) & 0xffU;
            crc >>= 8;
            crc ^= crc16_table[static_cast<size_t>(idx)];
        }

        ret = true;
    }
    else
    {
        DERR("error computing CRC frame too long " << +length << ">" << RHF_MAX_SIZE);
    }

    return ret;
}

//    bool OSEF::Raspihat::printFrame(const RHFrame& frame, const uint8_t& length)
//    {
//        bool ret = false;
//
//        if ((length >= RHF_MIN_SIZE) && (length <= RHF_MAX_SIZE))
//        {
//            std::cout << "id=0x" << std::hex << static_cast<char>(frame[static_cast<size_t>(0)]);
//            std::cout << " cmd=0x" << static_cast<char>(frame[static_cast<size_t>(1)]) << std::dec;
//            std::cout << " data=[";
//            for (ssize_t i = 2; i < static_cast<ssize_t>(length - 2); i++)
//            {
//                std::cout << " " << +frame[static_cast<size_t>(i)];
//            }
//            std::cout << " ]";
//            std::cout << " CRC=[" << static_cast<char>(frame[static_cast<size_t>(length-2)]) << " " << static_cast<char>(frame[static_cast<size_t>(length-1)]) << "]" << std::endl;
//
//            ret = true;
//        }
//
//        return ret;
//    }

bool OSEF::Raspihat::buildRequest(const uint8_t& reqid, const uint8_t& cmd, const RHFramePayload& pld, const uint8_t& length)
{
    bool ret = false;

    if (length <= RHF_PLD_MAX_SIZE)
    {
        request[0] = reqid;
        request[1] = cmd;

        if ( (length>static_cast<uint8_t>(0)) && (length <= (RHF_MAX_SIZE-2)) )
        {
            memcpy(&request[2], &pld[0], length);

            uint16_t crc = 0;
            if (getCRC16(crc, request, length+2))
            {
                request[static_cast<size_t>(length+2)] = static_cast<uint8_t>(crc & 0xffU);
                request[static_cast<size_t>(length+3)] = static_cast<uint8_t>( (crc >> static_cast<uint16_t>(8)) & static_cast<uint8_t>(0xff) );
                ret = true;
            }
        }
        else
        {
            uint16_t crc = 0;
            if (getCRC16(crc, request, length+2))
            {
                request[static_cast<size_t>(length+2)] = static_cast<uint8_t>(crc & 0xffU);
                request[static_cast<size_t>(length+3)] = static_cast<uint8_t>( (crc >> static_cast<uint16_t>(8)) & static_cast<uint8_t>(0xff) );
                ret = true;
            }
        }
    }
    else
    {
        DERR("error building request too long " << +length << ">" << RHF_PLD_MAX_SIZE);
    }

    return ret;
}

//    bool OSEF::Raspihat::writeRequest(const uint8_t& cmd, const RHFramePayload& pld, const uint8_t& sendl)
//    {
//        bool ret = false;
//
//        if (buildRequest(id, cmd, pld, sendl))
//        {
//            if (i2c_smbus_write_i2c_block_data(file, request[0], (sendl+RHF_OVH)-1, &(request[1])) == 0)  // id is sent as block command prefix
//            {
//                id++;
//                ret = true;
//            }
//            else
//            {
//                DERR("error writing request id=" << +id << " cmd=" << +cmd);
//            }
//        }
//
//        return ret;
//    }

bool OSEF::Raspihat::checkResponse(const uint8_t& respid, const uint8_t& cmd, const uint8_t& length)
{
    bool ret = false;

    if ((length >= RHF_MIN_SIZE) && (length <= RHF_MAX_SIZE))  // can't be more than 32 from readRequest
    {
        if ((respid == response[0]) && (cmd == response[1]))
        {
            uint16_t crc = 0;
            if (getCRC16(crc, response, length-2))
            {
                if ( (crc&0xffU) == response[static_cast<size_t>(length-2)])
                {
                    if (((crc >> static_cast<uint16_t>(8))& static_cast<uint8_t>(0xff)) == response[static_cast<size_t>(length-1)])
                    {
                        ret = true;
                    }
                    else
                    {
                        DERR("error checking response CRC16 high byte");
                    }
                }
                else
                {
                    DERR("error checking reponse CRC16 low byte");
                }
            }
        }
        else
        {
            DERR("malformed response id=" << +response[0] << " cmd=" << +response[1]);
        }
    }

    return ret;
}

bool OSEF::Raspihat::sendCommand(const uint8_t& cmd, const RHFramePayload& pld, const uint8_t& sendl, const uint8_t& recvl)
{
    bool ret = false;

    if (recvl <= RHF_PLD_MAX_SIZE)
    {
        if (buildRequest(id, cmd, pld, sendl))
        {
//            if (i2c_smbus_write_i2c_block_data(file, request[0], (sendl+RHF_OVH)-1, &(request[1])) == 0)  // id is sent as block command prefix
            I2CBlock writeBlock;
            memcpy(&writeBlock[0], &request[1], RHF_MAX_SIZE - 1);
            if (device.writeI2cBlockData(request[0], (sendl+RHF_OVH)-1, writeBlock))
            {
                id++;
//                int32_t rlen = i2c_smbus_read_i2c_block_data(file, 0xff, recvl+RHF_OVH, response);
//                if (rlen == (recvl+RHF_OVH))
                if (device.readI2cBlockData(0xff, recvl + RHF_OVH, response))
                {
                    ret = checkResponse(id-1, cmd, recvl + RHF_OVH);
                }
            }
            else
            {
                DERR("error writing request id=" << +id << " cmd=" << +cmd);
            }
        }
    }
    else
    {
        DERR("response payload too long " << +recvl << ">" << RHF_PLD_MAX_SIZE);
    }

    return ret;
}

bool OSEF::Raspihat::getBoardName(std::string& name)
{
    RHFramePayload data;
    const bool ret = sendCommand(0x10, data, 0, 25);
    if (ret)
    {
        for (size_t i = 0UL; i < static_cast<size_t>(25); i++)
        {
            if (response[RHF_HDR+i] != static_cast<uint8_t>('\0'))
            {
                name += std::to_string(response[RHF_HDR+i]);
            }
        }
    }
    return ret;
}

bool OSEF::Raspihat::getBoardVersion(uint8_t& maj, uint8_t& sub, uint8_t& min)
{
    RHFramePayload data;
    const bool ret = sendCommand(0x11, data, 0, 3);
    if (ret)
    {
        maj = response[RHF_HDR];
        sub = response[RHF_HDR+1];
        min = response[RHF_HDR+2];
    }
    return ret;
}

bool OSEF::Raspihat::getUInt32(const uint8_t& cmd, uint32_t& rcvdata)
{
    bool ret = false;
    RHFramePayload reqPayload;
    if (sendCommand(cmd, reqPayload, 0, 4))
    {
        if (memcpy(&rcvdata, &(response[RHF_HDR]), 4) == &rcvdata)
        {
            ret = true;
        }
    }
    return ret;
}

bool OSEF::Raspihat::setUInt32(const uint8_t& cmd, const uint32_t& snddata)
{
    bool ret = false;
    RHFramePayload reqPayload;
    if (memcpy(&reqPayload, &snddata, 4) == &reqPayload)
    {
        if (sendCommand(cmd, reqPayload, 4, 4))
        {
            uint32_t recvdata = 0;
            if (memcpy(&recvdata, &(response[RHF_HDR]), 4) == &recvdata)
            {
                if (snddata == recvdata)
                {
                    ret = true;
                }
                else
                {
                    DERR("wrong response power on states=" << snddata << "!=" << recvdata);
                }
            }
        }
    }

    return ret;
}

bool OSEF::Raspihat::getBoardstatus(uint32_t& status)
{
    const bool ret = getUInt32(0x12, status);
    return ret;
}

bool OSEF::Raspihat::resetBoard()
{
    RHFramePayload data;
    const bool ret = sendCommand(0x13, data, 0, 0);
    return ret;
}

bool OSEF::Raspihat::setRelayPowerOn(const uint32_t& states)
{
    const bool ret = setUInt32(0x30, states);
    return ret;
}

bool OSEF::Raspihat::getRelayPowerOn(uint32_t& states)
{
    const bool ret = getUInt32(0x31, states);
    return ret;
}

bool OSEF::Raspihat::setRelaySafety(const uint32_t& states)
{
    const bool ret = setUInt32(0x32, states);
    return ret;
}

bool OSEF::Raspihat::getRelaySafety(uint32_t& states)
{
    const bool ret = getUInt32(0x33, states);
    return ret;
}

bool OSEF::Raspihat::setRelayStates(const uint32_t& states)
{
    const bool ret = setUInt32(0x34, states);
    return ret;
}

bool OSEF::Raspihat::setRelayStates(const uint32_t& states, const uint32_t& mask)
{
    bool ret = false;

    uint32_t current = 0;
    if (getRelayStates(current))
    {
        ret = setUInt32(0x34, (current&~mask)|(states&mask));
    }

    return ret;
}

bool OSEF::Raspihat::getRelayStates(uint32_t& states)
{
    const bool ret = getUInt32(0x35, states);
    return ret;
}

bool OSEF::Raspihat::setRelayState(const uint8_t& channel, const bool& state)
{
    bool ret = false;
    RHFramePayload data;
    data[0] = channel;  // channel index
    data[1] = static_cast<uint8_t>(state);  // output value
    if (sendCommand(0x36, data, 2, 2))
    {
        if (channel == response[RHF_HDR])
        {
            if (state == (response[RHF_HDR+1] > 0))
            {
                ret = true;
            }
            else
            {
                DERR("wrong response state=" << state << "!=" << +response[RHF_HDR+1]);
            }
        }
        else
        {
            DERR("wrong response channel=" << +channel << "!=" << +response[RHF_HDR]);
        }
    }
    return ret;
}

bool OSEF::Raspihat::getRelayState(const uint8_t& channel, bool& state)
{
    bool ret = false;
    RHFramePayload data;
    data[0] = channel;  // channel index
    if (sendCommand(0x37, data, 1, 2))
    {
        if (channel == response[RHF_HDR])
        {
            state = (response[RHF_HDR+1] != 0U);
            ret = true;
        }
        else
        {
            DERR("wrong response channel=" << +channel << "!=" << +response[RHF_HDR]);
        }
    }
    return ret;
}

bool OSEF::Raspihat::getInputs(uint32_t& inputs)
{
    const bool ret = getUInt32(0x20, inputs);
    return ret;
}

bool OSEF::Raspihat::getInput(const uint8_t& channel, bool& input)
{
    RHFramePayload data;
    data[0] = channel;  // channel index
    const bool ret = sendCommand(0x21, data, 1, 2);
    input = (response[RHF_HDR+1] != 0U);
    return ret;
}

bool OSEF::Raspihat::getInputCounter(const uint8_t& channel, const bool& edge, uint32_t& counter)
{
    bool ret = false;
    RHFramePayload data;
    data[0] = channel;  // channel index
    data[1] = static_cast<uint8_t>(edge);  // counter edge 0=falling 1=rising
    if (sendCommand(0x22, data, 2, 6))
    {
        if (response[RHF_HDR] == channel)
        {
            if ((response[RHF_HDR+1] > 0) == edge)
            {
                if (memcpy(&counter, &(response[RHF_HDR+2]), 4) == &counter)
                {
                    ret = true;
                }
            }
            else
            {
                DERR("wrong response edge=" << +edge << "!=" << +response[RHF_HDR+1]);
            }
        }
        else
        {
            DERR("wrong response channel=" << +channel << "!=" << +response[RHF_HDR]);
        }
    }
    return ret;
}

bool OSEF::Raspihat::resetInputCounter(const uint8_t& channel, const bool& edge)
{
    bool ret = false;
    RHFramePayload data;
    data[0] = channel;  // channel index
    data[1] = static_cast<uint8_t>(edge);  // counter edge 0=falling 1=rising
    if (sendCommand(0x23, data, 2, 2))
    {
        if (response[RHF_HDR] == channel)
        {
            if ((response[RHF_HDR+1] > 0) == edge)
            {
                ret = true;
            }
            else
            {
                DERR("wrong response edge=" << edge << "!=" << +response[RHF_HDR+1]);
            }
        }
        else
        {
            DERR("wrong response channel=" << +channel << "!=" << +response[RHF_HDR]);
        }
    }
    return ret;
}

bool OSEF::Raspihat::resetInputCounters()
{
    RHFramePayload data;
    const bool ret = sendCommand(0x24, data, 0, 0);
    return ret;
}
