#include "Raspihat.h"
#include "Debug.h"

#include <unistd.h>  // getopt
#include <iostream>

typedef struct
{
    uint8_t adapter;
    uint8_t device;
    uint8_t input;
}RaspihatInputOptions;

bool getOptionUInt8(uint8_t& u8, const uint8_t& min, const uint8_t& max)
{
    bool ret = false;

    uint64_t tmp = std::strtoul(optarg, nullptr, 10);
    if ((tmp >= min) && (tmp <= max))
    {
        u8 = tmp;

        ret = true;
    }

    return ret;
}

bool getOptionBool(bool& b)
{
    bool ret = false;

    uint64_t tmp = std::strtoul(optarg, nullptr, 10);
    if (tmp <= 1UL)
    {
        if (tmp == 1)
        {
            b = true;
        }
        else
        {
            b = false;
        }

        ret = true;
    }

    return ret;
}

bool getRaspihatInputOptions(int argc, char** argv, RaspihatInputOptions& options)
{
    bool ret = true;

    if (argc > 1)
    {
        int32_t opt = -1;
        do
        {
            opt = getopt(argc, argv, "a:d:i:");
            if (opt != -1)
            {
                switch (opt)
                {
                    case 'a':
                        ret = getOptionUInt8(options.adapter, 1, 255);
                        break;
                    case 'd':
                        ret = getOptionUInt8(options.device, 0, 255);
                        break;
                    case 'i':
                        ret = getOptionUInt8(options.input, 0, 15);
                        break;
                    default:
                        ret = false;
                        break;
                }
            }
        }while (ret && (opt != -1));
    }

    if (not ret)
    {
        std::cout << "Usage:   " << *argv << " [-option] [argument]" << std::endl;
        std::cout << "options:  " << std::endl;
        std::cout << "         " << "-a  I2C adapter (1) [1-255]" << std::endl;
        std::cout << "         " << "-d  I2C device (0x60) [0-255]" << std::endl;
        std::cout << "         " << "-i  input (0) [0-15]" << std::endl;
    }

    return ret;
}

int main(int argc, char** argv)
{
    int32_t ret = -1;

    RaspihatInputOptions options = {1, 0x60, 0};
    if (getRaspihatInputOptions(argc, argv, options))
    {
        OSEF::Raspihat hat(options.adapter, options.device);

        bool readState = false;
        if (hat.getInput(options.input, readState))
        {
            std::cout << readState << std::endl;
            ret = 0;
        }
    }

    return ret;
}
