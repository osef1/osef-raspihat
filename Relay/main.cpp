#include "Raspihat.h"
#include "Debug.h"

#include <unistd.h>  // getopt
#include <iostream>

typedef struct
{
    uint8_t adapter;
    uint8_t device;
    uint8_t relay;
    bool state;
}RaspihatRelayOptions;

bool getOptionUInt8(uint8_t& u8, const uint8_t& min, const uint8_t& max)
{
    bool ret = false;

    uint64_t tmp = std::strtoul(optarg, nullptr, 10);
    if ((tmp >= min) && (tmp <= max))
    {
        u8 = tmp;

        ret = true;
    }

    return ret;
}

bool getOptionBool(bool& b)
{
    bool ret = false;

    uint64_t tmp = std::strtoul(optarg, nullptr, 10);
    if (tmp <= 1UL)
    {
        if (tmp == 1)
        {
            b = true;
        }
        else
        {
            b = false;
        }

        ret = true;
    }

    return ret;
}

bool getRaspihatRelay(int argc, char** argv, RaspihatRelayOptions& options)
{
    bool ret = true;

    if (argc > 1)
    {
        int32_t opt = -1;
        do
        {
            opt = getopt(argc, argv, "a:d:r:s:");
            if (opt != -1)
            {
                switch (opt)
                {
                    case 'a':
                        ret = getOptionUInt8(options.adapter, 1, 255);
                        break;
                    case 'd':
                        ret = getOptionUInt8(options.device, 0, 255);
                        break;
                    case 'r':
                        ret = getOptionUInt8(options.relay, 0, 9);
                        break;
                    case 's':
                        ret = getOptionBool(options.state);
                        break;
                    default:
                        ret = false;
                        break;
                }
            }
        }while (ret && (opt != -1));
    }

    if (not ret)
    {
        std::cout << "Usage:   " << *argv << " [-option] [argument]" << std::endl;
        std::cout << "option:  " << std::endl;
        std::cout << "         " << "-a  I2C adapter (1) [1-255]" << std::endl;
        std::cout << "         " << "-d  I2C device (0x60) [0-255]" << std::endl;
        std::cout << "         " << "-r  relay (0) [0-9]" << std::endl;
        std::cout << "         " << "-s  state (0) [0-1]" << std::endl;
    }

    return ret;
}

int main(int argc, char** argv)
{
    int32_t ret = -1;

    RaspihatRelayOptions options = {1, 0x60, 0, false};
    if (getRaspihatRelay(argc, argv, options))
    {
        OSEF::Raspihat hat(options.adapter, options.device);

        if (hat.setRelayState(options.relay, options.state))
        {
            bool readState = false;
            if (hat.getRelayState(options.relay, readState))
            {
                if (options.state == readState)
                {
                    DOUT("relay " << +options.relay << " state is " << readState);
                    ret = 0;
                }
                else
                {
                    DWARN("relay " << +options.relay << " state is " << readState);
                }
            }
        }
    }

    return ret;
}
